import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class CalculatorTest {
    
    /*public static void main(String[] args){
        evaluatesExpression();
    }*/
  @Test
  public  void evaluatesExpression() {
    Calculator calculator = new Calculator();
    int sum = calculator.evaluate("1+2+3");
    assertEquals(6, sum);
  }
}